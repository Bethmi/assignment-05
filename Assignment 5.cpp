#include<stdio.h>

int	NumberOfAttendees(int p){
	return 120-((p-15)/5*20);	
}
int Revenue(int p){
	return NumberOfAttendees(p)*p;
}
// performance cost Rs.500 (fixed cost) and per attendee Rs.3 (variable cost)
int Cost(int p){
	return (NumberOfAttendees(p)* 3) + 500;
}
int Profit(int p){
	return Revenue(p)-Cost(p);
}
int main(){
	int p;
	printf("Ticket Price         \t Expected Profit \n\n");
	for(p=0;p<55;p+=5)
	{
		printf("   Rs.%d       \t---->\t     Rs.%d\n",p,Profit(p));
	
    } 
		return 0;	
	}

